PySCF联网在线安装只需`pip install pyscf`一行命令即可，能联网的建议通过联网安装。本文介绍的是离线安装步骤，适合不允许联网或很难联网的内部节点。读者在开始编译前需确认自己机子上有gcc和g++编译器，有MKL数学库，以及cmake软件。运行如下命令可查看自己机子上是否存在
```
which gcc
which g++
which cmake
cmake --version (查看版本号)
echo $MKLROOT
```
笔者撰文时用的gcc版本为4.8.5（更高版本亦可），cmake版本为3.19（不能低于3.5），MKL数学库用的是Intel Parallel Studio XE 2019 update 5里的（更高版本的更好，最新的Intel OneAPI也可），Python用的是Anaconda Python 3，版本为3.7.6（不推荐低于3.7）。读者机子上若缺少任一前提条件请自行安装。

# 1.下载
先到[官网](https://github.com/pyscf/pyscf/releases)下载PySCF压缩包，本文以2.1.1版为例，但安装过程对所有2.x版均适用。下载得到pyscf-2.1.1.tar.gz。解压，进入lib目录
```
tar -zxf pyscf-2.1.1.tar.gz
cd pyscf-2.1.1/pyscf/lib
```
打开此目录下的CMakeLists.txt文件，搜索URL或GIT_REPOSITORY可以看到所需三个库的网址和版本号，然后到对应网址去下载相应版本的压缩包即可。方便起见，这里把网址也贴出来
```
https://github.com/sunqm/libcint/archive/refs/tags/v5.1.5.tar.gz
https://gitlab.com/libxc/libxc/-/archive/5.2.0/libxc-5.2.0.tar.gz
https://github.com/fishjojo/xcfun/archive/refs/tags/cmake-3.5.tar.gz
```
笔者按照这些网址下载下来的压缩包分别是
```
libcint-5.1.5.tar.gz
libxc-5.2.0.tar.gz
xcfun-cmake-3.5.tar.gz
```
**注意：不是随便什么版本、或最新版本的库都能搭配pyscf-2.1.1。经常有读者阅读本教程后随意发挥，库版本随意搭配，然后来问笔者怎么安装不了**。版本搭配见下表
| PySCF | libcint | libxc | xcfun |
| --- | --- | --- | --- |
| 1.7.6 | 4.0.7 | 4.3.4 | cmake-3.5 |
| 2.0.1 | 4.4.6 | 5.1.7 | cmake-3.5 |
| 2.1.1 | 5.1.5 | 5.2.0 | cmake-3.5 |
| 2.2.1 | 5.3.0 | 6.1.0 | cmake-3.5 |

# 2.编译libcint
到存放压缩包的目录下（与刚刚pyscf-2.1.1/pyscf/lib目录可以无关），依次执行
```
tar -zxf libcint-5.1.5.tar.gz
cd libcint-5.1.5
mkdir build && cd build

cmake -DWITH_F12=1 -DWITH_RANGE_COULOMB=1 -DWITH_COULOMB_ERF=1 \
-DMIN_EXPCUTOFF=20 -DKEEP_GOING=1 \
-DCMAKE_INSTALL_PREFIX:PATH=$HOME/software/cint_and_xc \
-DCMAKE_INSTALL_LIBDIR:PATH=lib ..

make -j8
make install
```
这里的库存放路径`$HOME/software/cint_and_xc`是笔者的个人偏好，读者可以按照自己的喜好或需求更改。-j8表示8核并行编译，若读者机子没有8核，则应去掉-j8。

# 3.编译libxc
到存放压缩包的目录下，依次执行
```
tar -zxf libxc-5.2.0.tar.gz
cd libxc-5.2.0
mkdir build && cd build

cmake -DCMAKE_BUILD_TYPE=RELEASE -DBUILD_SHARED_LIBS=1 \
-DCMAKE_INSTALL_PREFIX:PATH=$HOME/software/cint_and_xc \
-DCMAKE_INSTALL_LIBDIR:PATH=lib ..

make -j8
make install
```

# 4.编译xcfun
到存放压缩包的目录下，依次执行
```
tar -zxf xcfun-cmake-3.5.tar.gz
cd xcfun-cmake-3.5
mkdir build && cd build

cmake -DCMAKE_BUILD_TYPE=RELEASE -DBUILD_SHARED_LIBS=1 \
-DXCFUN_MAX_ORDER=3 \
-DCMAKE_INSTALL_PREFIX:PATH=$HOME/software/cint_and_xc \
-DCMAKE_INSTALL_LIBDIR:PATH=lib ..

make -j8
make install
```
上述四步完成后，可以在$HOME/software/cint_and_xc下发现有bin、include、share和lib四个文件夹。接着将lib路径添加进环境变量，即打开~/.bashrc文件写入
```
export LD_LIBRARY_PATH=$HOME/software/cint_and_xc/lib:$LD_LIBRARY_PATH
export CPATH=$HOME/software/cint_and_xc/include:$CPATH
```
然后运行`source ~/.bashrc`使之生效。几个库的tar.gz压缩包和解压出来的文件夹都可以删了，保留cint_and_xc文件夹即可.

# 5.安装PySCF
我们回到本文一开始提到的pyscf-2.1.1/pyscf/lib目录，再次打开CMakeLists.txt文件，找到第一个# set(BLAS_LIBRARIES这一行，在此处删除注释符号“# ”（注意是井号和空格两个字符），更改mkl库路径为当前系统下的mkl路径，例如笔者机子上的是
```
/opt/intel/mkl/lib/intel64
```
注意别把前头的-L删了，也别把后面一堆-lmkl_xxx删了。如果读者用的是Intel OneAPI，MKL库路径略有不同，这里举一个例子
```
/opt/intel/oneapi/mkl/latest/lib/intel64
```
读者需根据自己机子实际情况修改。注意下面几行“or”部分不用动。接着执行
```
mkdir build && cd build
cmake -DBUILD_LIBCINT=0 -DBUILD_LIBXC=0 -DBUILD_XCFUN=0 \
-DCMAKE_INSTALL_PREFIX:PATH=$HOME/software/cint_and_xc ..

make
```
安装完成后将PySCF的路径添加进~/.bashrc文件：
```
export PYTHONPATH=$HOME/software/pyscf-2.1.1:$PYTHONPATH
```
完成安装。最后同样要记得执行source ~/.bashrc，或者退出重登。

# 6.测试例子
随便测试个CCSD(T)计算。新建一个文件a.py，写入
```
from pyscf import gto, scf, cc
mol = gto.M(atom='H 0 0 0; F 0 0 1.1',basis='ccpvdz')
mf = scf.RHF(mol)
mf.kernel()

mycc = cc.CCSD(mf)
mycc.frozen = 1
mycc.kernel()

mycc.ccsd_t()
```
保存。运行
```
python a.py
```
输出
```
converged SCF energy = -99.9873974403487
E(CCSD) = -100.2018830963878  E_corr = -0.2144856560390928
CCSD(T) correction = -0.00239622928398965
```
读者可以自行用高斯做个计算对比。注意高斯在电子相关计算中默认冻结芯轨道，而PySCF默认不冻结。对于氟化氢这个例子需要冻结的轨道只有1个，即F原子的1s轨道，因此这里显式地设定了mycc.frozen = 1让其与高斯一致。

# 7.安装dmrgscf,shciscf等extension
这在pyscf-1.7.6（及更旧版本）里是内置好的module，无需安装。如果读者对DMRG,SHCI计算不感兴趣，则不用安装，可跳过这一步。这个文件夹从pyscf-2.0开始被移出，需要用户手动下载，安装方法见[离线安装PySCF-2.x-extensions](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85PySCF-2.x-extensions.md)

# 8.可能遇到的问题
（1）如果编译时报错找不到libmkl_avx.so，并且你使用的是Intel OneAPI 2021.x版本，且你在`$MKLROOT/lib/intel64/`目录下能找到libmkl_avx.so.1，却找不到libmkl_avx.so，这时需要你手动创建软链接，即
```
cd /opt/intel/oneapi/mkl/latest/lib/intel64/
ln -s libmkl_avx.so.1 libmkl_avx.so
```
上述操作需要管理员权限（若Intel编译器没装在/opt/下，而是装在用户目录下，则无需管理员权限），然后再编译PySCF。注意这点与下述（2）是不同的问题，不能混为一谈。若没有libmkl_avx.so.1但有libmkl_avx.so.2，创建软链接时原文件使用libmkl_avx.so.2即可。  
（2）如果运行时报错找不到库mkl_def.so和mkl_avx2.so，可回到上述第5点中再次打开CMakeLists.txt文件，在-lmkl_avx后添加-lmkl_def -lmkl_avx2，保存。进入build目录，从本文第5点cmake处重新开始（含cmake操作）。  
（3）如果编译PySCF中途更换了任何相关的编译器或库版本，应先在build/下执行`make clean`清理上次编译残留文件，然后再进行编译。  
（4）若使用Intel 2019 update 3搭配Anaconda Python 3.8.x编译PySCF，运行时可能会报错`libmkl_avx.so: undefined symbol: mkl_dnn_getTtl_F32`，笔者没什么优美的解决办法，但笔者经过大量尝试，发现安装Anaconda Python 3.7.6（即降版本）或安装更高版本Intel编译器可解决该问题。各个版本的Anaconda Python可以到[镜像网站](http://mirror.nju.edu.cn/anaconda/archive)下载，其中文件名和版本号的对应关系为  
> Anaconda3-2020.02-Linux-x86_64.sh <-> Python 3.7.6  
> Anaconda3-2020.07-Linux-x86_64.sh <-> Python 3.8.3  
> Anaconda3-2020.11-Linux-x86_64.sh <-> Python 3.8.5  
> Anaconda3-2021.05-Linux-x86_64.sh <-> Python 3.8.8  
> Anaconda3-2022.05-Linux-x86_64.sh <-> Python 3.9.12

（5）比较新的Intel MKL或OneAPI可能在PySCF运行时出现以下问题：
```
INTEL MKL ERROR: /xxx/libmkl_def.so.2: undefined symbol: mkl_sparse_optimize_bsr_trsm_i8
INTEL MKL FATAL ERROR: Cannot load libmkl_def.so.2
```
这可以通过添加环境变量`export LD_PRELOAD=$MKLROOT/lib/intel64/libmkl_core.so:$MKLROOT/lib/intel64/libmkl_sequential.so`来解决。

## 相关阅读
[离线安装PySCF-1.7.6](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85PySCF-1.7.6.md)
