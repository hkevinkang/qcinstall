本文适用于OpenMolcas-v22.02和v22.06，对以后的版本可能适用也可能不适用。旧版OpenMolcas、与QCMaquis联用版本的安装请见文末链接。安装前我们需要确保有必要的编译器和库：  
cmake >= 3.12，Intel编译器（含MKL），python  
笔者机子上安装的分别是cmake 3.19、Intel 2019 update5和Anaconda Python3。尽管可以使用GNU编译器替代Intel，但笔者的个人使用经验表明用Intel编译器快不少。若读者未装Intel编译器，可以参看[Linux下安装Intel oneAPI](https://mp.weixin.qq.com/s/7pQETkrDO1C83vQjKQqI4w)一文。

# 1.下载
到GitLab下载[OpenMolcas](https://gitlab.com/Molcas/OpenMolcas/-/releases)，笔者下载的压缩包是OpenMolcas-v22.06.tar.gz。从v22.02开始，编译OpenMolcas要求有Libxc库，所以我们还需到GitLab下载[Libxc](https://gitlab.com/libxc/libxc/-/releases)，笔者下载的压缩包是libxc-5.2.2.tar.gz。

# 2.编译和安装
进入存放压缩包的目录，解压，配置
```
tar -zxf OpenMolcas-v22.06.tar.gz
cd OpenMolcas-v22.06
mkdir bin build
cd build
CC=icc CXX=icpc FC=ifort cmake -DLINALG=MKL -DOPENMP=ON \
-DCMAKE_INSTALL_PREFIX=/home/$USER/software/OpenMolcas-v22.06 ..
```
此时不要急着执行make。我们需要把Libxc放到相应目录下，让OpenMolcas自动识别并编译它。依次执行
```
cd External/Libxc/tmp
rm -f Libxc-gitclone.cmake Libxc-gitupdate.cmake
touch Libxc-gitclone.cmake Libxc-gitupdate.cmake
cd ../src
cp ~/software/libxc-5.2.2.tar.gz .
tar -zxf libxc-5.2.2.tar.gz
rm -rf Libxc libxc-5.2.2.tar.gz
mv libxc-5.2.2 Libxc
cd ../../../
```
注意笔者将libxc-5.2.2.tar.gz压缩包放在~/software/目录下，所以是从~/software/拷贝。若读者放在其他目录下，则应从其他目录拷贝到External/Libxc/src/下。笔者的习惯是将软件放在/home/$USER/software/目录下，读者也可以放到其他位置。然后编译
```
make -j24
make install
```
24核编译仅需约1 min。正常结束后执行
```
cd ..
mv pymolcas bin/
```
即将脚本pymolcas移入我们之前新建的bin目录里。接着在~/.bashrc中写上OpenMolcas环境变量
```
# OpenMolcas-v22.06
export MOLCAS_WORKDIR=/scratch/$USER/molcas
export MOLCAS_MEM=32Gb
export MOLCAS=/home/$USER/software/OpenMolcas-v22.06
export PATH=$MOLCAS/bin:$PATH
export MOLCAS_PRINT=3
export MOLCAS_NPROCS=1
export OMP_NUM_THREADS=24
```
完成后记得执行source ~/.bashrc或退出重登。各种路径可以根据读者自己机子的实际情况进行修改。若/scratch/$USER/molcas目录不存在则需手动创建。无论计算任务正常/异常结束，该目录都会有临时文件存在，每隔一段时间应进行清理。若一个任务算完后在相同位置再次提交，OpenMolcas默认去寻找上次的临时文件，好处是可以加速计算，坏处是万一不想要临时文件里的轨道作为初猜，就可能把自己坑了。若想每次计算完自动清空临时文件，可以再加上环境变量
```
export MOLCAS_KEEP_WORKDIR=NO
```
变量MOLCAS_PRINT=3可以让输出内容更多一些，偶尔有小伙伴向笔者反映他们的输出内容比我少，往往就是这个参数造成的。变量MOLCAS_NPROCS用于MPI并行，但本文编译的是MKL并行版，不支持MPI并行，因此设为1。笔者的节点上有24核，因此OMP并行核数设置为24。这些环境变量仅是笔者的个人推荐，并非适用于任何机器，详细的环境变量说明请阅读OpenMolcas手册
```
https://molcas.gitlab.io/OpenMolcas/Manual.pdf
```

# 3.测试
执行
```
pymolcas --version
```
屏幕应当显示
```
python driver version = py2.23
(after the original perl EMIL interpreter of Valera Veryazov)
```
接着测试程序自带标准示例。最好切换到其他目录进行测试
```
cd ~/
pymolcas verify standard
```
这个standard其实对应OpenMolcas-v22.06/test/standard目录，内含102个输入文件，可供自学模仿使用。测试过程中输出内容仅有一行，例如
```
Running test standard: 067... (68%)
```
测试时间可能长达1小时。如果嫌测试时间长，可以自己挑十几个文件测试，例如测试第004，005号文件的命令是
```
pymolcas verify standard:004,005
```

# 4.OpenMolcas与其他量化程序传递轨道
笔者开发的开源、免费程序[MOKIT](https://gitlab.com/jxzou/mokit)可以在常见量子化学程序间传递分子轨道（见文末链接1，第4节），并调用常见量子化学程序自动做多参考态计算（见文末链接3），本文不再重复介绍。


## 相关阅读
1. [量子化学程序OpenMolcas的简易安装(低于v22.02)](https://mp.weixin.qq.com/s/wA8YClRxkRTChtQvuum-Uw)  
2. [离线编译OpenMolcas+QCMaquis](https://mp.weixin.qq.com/s/Gb1Lzv1bcQmvuHMZjAQLxQ) 
3. [自动做多参考态计算的程序MOKIT](https://mp.weixin.qq.com/s/bM244EiyhsYKwW5i8wq0TQ)
