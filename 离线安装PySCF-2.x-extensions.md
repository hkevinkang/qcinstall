# 1.安装dmrgscf库
这在pyscf-1.7.6（及更旧版本）里是内置好的一个文件夹，无需安装。如果读者对DMRG计算不感兴趣，则不用安装，可跳过这一步。这个文件夹从pyscf-2.0开始被移出，需要用户手动下载，网址为
```
https://github.com/orgs/pyscf/repositories?type=all
```
这上面有许多其他与pyscf有接口的库。点进dmrgscf，下载压缩包。为方便起见，这里直接给出网址
```
https://github.com/pyscf/dmrgscf/archive/refs/heads/master.zip
```
下载下来是dmrgscf-master.zip。解压，重命名
```
unzip dmrgscf-master.zip
mv dmrgscf-master dmrgscf
```
进入目录，编译
```
cd dmrgscf/
python setup.py build
```
如果此时报错`ImportError: cannot import name 'find_namespace_packages'`，说明Anaconda Python 3版本过低，不含该库。笔者尝试过发现Anaconda Python 3.6会有此问题，>=3.7则没有。若读者中途更换或更新Anaconda Python 3，最好从第5步开始重新编译PySCF。接着我们把libunpack库移到pyscf的lib目录里
```
mv build/lib.linux-x86_64-3.7/pyscf/lib/libunpack.cpython-37m-x86_64-linux-gnu.so ~/software/pyscf-2.1.1/pyscf/lib/
```
注意libunpack库的路径和名称会随Python版本的不同而不同。移到pyscf的lib目录里是一种偷懒做法，这样我们就不需要对这个库路径写环境变量了。最后，还需在~/.bashrc中添加一个叫PYSCF_EXT_PATH的环境变量
```
export PYSCF_EXT_PATH=/home/$USER/software/dmrgscf:$PYSCF_EXT_PATH
```
这个库的安装就完成了。不过，仅靠PySCF和这个文件夹仍不足以直接进行DMRG计算，还需安装[Block-1.5.3](https://mp.weixin.qq.com/s/EUZKLYSqbuIUL9-zlySfbQ)或[pyblock2](https://gitlab.com/jxzou/qcinstall/-/blob/main/block2%E7%9A%84%E7%BC%96%E8%AF%91%E5%92%8C%E5%AE%89%E8%A3%85.md)。

# 2.安装shciscf库
这在pyscf-1.7.6（及更旧版本）里是内置好的一个文件夹，无需安装。如果读者对SHCI计算不感兴趣，则不用安装，可跳过这一步。这个文件夹从pyscf-2.0开始被移出，需要用户手动下载，网址为
```
https://github.com/pyscf/shciscf/archive/refs/heads/master.zip
```
下载下来是shciscf-master.zip。解压，重命名
```
unzip shciscf-master.zip
mv shciscf-master shciscf
```
进入目录，编译
```
cd shciscf/
python setup.py build
```
如果此时报错`ImportError: cannot import name 'find_namespace_packages'`，可参照上一节的办法处理。接着我们移动一下libshciscf库
```
mv build/lib.linux-x86_64-3.7/pyscf/lib/libshciscf.so ./pyscf/lib/
```
注意libshciscf库的路径和名称会随Python版本的不同而不同。最后，还需在~/.bashrc中添加一个叫PYSCF_EXT_PATH的环境变量
```
export PYSCF_EXT_PATH=/home/$USER/software/shciscf:$PYSCF_EXT_PATH
```
这个库的安装就完成了。不过，仅靠PySCF和这个文件夹仍不足以直接进行SHCI计算，还需安装[Dice](https://sanshar.github.io/Dice/)。



