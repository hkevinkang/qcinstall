到官网
```
https://www.open-mpi.org/software/ompi/v4.1
```
下载压缩包，笔者下载的压缩包为openmpi-4.1.1.tar.bz2。解压，进入程序目录
```
tar -jxf openmpi-4.1.1.tar.bz2
cd openmpi-4.1.1
```

从以下三种配置方式中选择一种：

（1）若想使用默认的GCC编译器编译Open MPI，则运行
```
./configure --prefix=$HOME/software/openmpi-4.1.1
```

（2）若安装了[高版本GCC](https://gitlab.com/jxzou/qcinstall/-/blob/main/Linux%E4%B8%8B%E5%AE%89%E8%A3%85%E9%AB%98%E7%89%88%E6%9C%ACGCC.md)，但发现没有被自动识别，可指明编译器路径，例如
```
./configure --prefix=$HOME/software/openmpi-4.1.1 \
FC=/opt/gcc-5.4.0/bin/gfortran CC=/opt/gcc-5.4.0/bin/gcc \
CXX=/opt/gcc-5.4.0/bin/g++
```

（3）若想使用Intel编译器编译Open MPI，则运行
```
./configure --prefix=$HOME/software/openmpi-4.1.1 FC=ifort CC=icc CXX=icpc
```

选择上述任一种配置方式后，运行
```
make -j24
make install
```
此处使用24核并行编译，读者需根据自己机器情况修改核数。完成后在`~/.bashrc`中写上环境变量
```
export PATH=$HOME/software/openmpi-4.1.1/bin:$PATH
export LD_LIBRARY_PATH=$HOME/software/openmpi-4.1.1/lib:$LD_LIBRARY_PATH
export CPATH=$HOME/software/openmpi-4.1.1/include:$CPATH
```
执行`source ~/.bashrc`或退出重登，以使环境变量生效。

