近来不少小伙伴向笔者反映OpenMP并行版OpenMolcas编译虽然很简单，但是算较大的体系速度较慢，建议笔者尝试
编译MPI并行版OpenMolcas并推出安装教程。本文测试时使用OpenMolcas-v22.10，对v22.06版本也适用，笔者未测
试其他版本。安装前我们需要确保有必要的编译器和库：  
cmake >= 3.12，Intel编译器（含MKL），python  
笔者机子上安装的分别是cmake 3.22、Intel OneAPI和Anaconda Python 3.8.8。对Intel编译器和Anaconda Python
版本一般不敏感，例如使用Intel Parallel Studio XE 2019亦可。

# 1.关于MPI的说明
常见的MPI有Open MPI, Intel MPI和MPICH等等。笔者测试过Open MPI的常见版本（1.10.3, 4.1.1等）都能用于编
译OpenMolcas并且正常使用，但笔者机器上使用Intel MPI时虽然可以编译OpenMolcas，但是运行计算时连输出内容
和报错信息都没有，任务就异常终止了，因此暂不考虑Intel MPI。另外要注意Open MPI、下文中的Global Array (GA)
库和OpenMolcas都应采用同一种编译器进行编译，例如全用Intel编译器，最好不要Intel/GNU混用。如果读者不清楚
机器上是否装过，可运行如下指令查看
```
type mpicc
type mpicxx
```
若已安装，则可跳过此步。若未安装，可采用如下安装步骤(以openmpi-1.10.3为例)
```
tar -zxf openmpi-1.10.3.tar.gz
cd openmpi-1.10.3
./configure --prefix=$HOME/software/openmpi_1_10_3 FC=ifort CC=icc CXX=icpc
make -j16
make install
```
完成后压缩包openmpi-1.10.3.tar.gz和解压出的文件夹openmpi-1.10.3都可删去，可执行程序和动态库在
openmpi_1_10_3/目录下。需要在`~/.bashrc`文件里写上相应环境变量
```
export PATH=$HOME/software/openmpi_1_10_3/bin:$PATH
export LD_LIBRARY_PATH=$HOME/software/openmpi_1_10_3/lib:$LD_LIBRARY_PATH
export CPATH=/$HOME/software/openmpi_1_10_3/include:$CPATH
```
然后退出重登，以使环境变量生效。

# 2.下载压缩包
到GitLab下载OpenMolcas
```
https://gitlab.com/Molcas/OpenMolcas/-/releases
```
笔者下载的压缩包是OpenMolcas-v22.10.tar.gz。从v22.02开始，编译OpenMolcas要求有Libxc库，
所以我们还需到GitLab下载Libxc
```
https://gitlab.com/libxc/libxc/-/releases
```
笔者下载的压缩包是libxc-5.2.2.tar.gz。要编译MPI并行版OpenMolcas还需有GA库，可到GitHub上下载
```
https://github.com/GlobalArrays/ga/releases
```
笔者下载的是ga-5.8.1.tar.gz。注意在编译OpenMolcas时cmake步骤中开启选项`-DMPI=ON`若没有GA库，
编译时不会报错，但编译出的程序实际上不支持MPI并行，因此我们需要先编译GA库。

# 3.编译GA库
解压，进入目录，配置和编译
```
tar -zxf ga-5.8.1.tar.gz
cd ga-5.8.1
./configure --enable-i8 --prefix=$HOME/software/ga_5_8_1 --with-blas8=-mkl \
--with-scalapack8="-L$MKLROOT/lib/intel64 -lmkl_scalapack_ilp64 -lmkl_intel_ilp64
                   -lmkl_intel_thread -lmkl_core -lmkl_blacs_intelmpi_ilp64
                   -liomp5 -lpthread -lm" \
CC=icc CXX=icpc F77=ifort FC=ifort MPICC=mpicc MPICXX=mpicxx \
MPIF77=mpif77 MPIFC=mpif90

make -j24
make install
```
笔者的习惯是将程序安装进`$HOME/software/`目录。笔者尝试过偷懒写`--with-scalapack8=-mkl`
发现没法自动识别机器上MKL中的scalapack，只好把一长串内容全写上去。上述命令写明了GA目录名是
ga_5_8_1，完成后可删除ga-5.8.1文件夹和压缩包ga-5.8.1.tar.gz，接着在~/.bashrc文件里
写上环境变量
```
export GAROOT=$HOME/software/ga_5_8_1
export LD_LIBRARY_PATH=$GAROOT/lib64:$LD_LIBRARY_PATH
export CPATH=$GAROOT/include:$CPATH
```
然后执行`source ~/.bashrc`使之生效或直接退出重登。

# 4.编译MPI版OpenMolcas
进入存放OpenMolcas压缩包的目录，解压，配置
```
tar -zxf OpenMolcas-v22.10.tar.gz
cd OpenMolcas-v22.10
mkdir bin build
cd build
CC=icc CXX=icpc F77=ifort FC=ifort MPICC=mpicc MPICXX=mpicxx \
cmake -DLINALG=MKL -DMPI=ON -DGA=ON \
-DMPIEXEC_EXECUTABLE=/opt/openmpi-1.10.3/bin/mpiexec \
-DCMAKE_INSTALL_PREFIX=$HOME/software/OpenMolcas-v22.10 ..
```
注意这里笔者使用了`MPIEXEC_EXECUTABLE`变量指定机器上的Open MPI路径，否则若机器上安装
有多种MPI极有可能自动识别成其他MPI，因而直接指定较为保险。读者需根据自己机器的实际情况
修改此路径。接下来不要急着执行make。我们需要把Libxc放到相应目录下，让OpenMolcas自动识别
并编译它。依次执行
```
cd External/Libxc/tmp
rm -f Libxc-gitclone.cmake Libxc-gitupdate.cmake
touch Libxc-gitclone.cmake Libxc-gitupdate.cmake
cd ../src
cp ~/software/libxc-5.2.2.tar.gz .
tar -zxf libxc-5.2.2.tar.gz
rm -rf Libxc libxc-5.2.2.tar.gz
mv libxc-5.2.2 Libxc
cd ../../../
```
注意笔者将libxc-5.2.2.tar.gz压缩包放在~/software/目录下，所以是从~/software/拷贝。若
读者放在其他目录下，则应从其他目录拷贝到External/Libxc/src/下。然后编译
```
make -j24
make install
```
24核并行编译需约2 min。正常结束后执行
```
cd ..
mv pymolcas bin/
```
即将脚本pymolcas移入我们之前新建的bin目录里。接着在~/.bashrc中写上OpenMolcas环境变量
```
# OpenMolcas
export MOLCAS_WORKDIR=/scratch/$USER/molcas
export MOLCAS_MEM=2Gb
export MOLCAS=$HOME/software/OpenMolcas-v22.10
export PATH=$MOLCAS/bin:$PATH
export MOLCAS_PRINT=3
export MOLCAS_NPROCS=24
```
完成后记得执行source ~/.bashrc或退出重登。各种路径可以根据读者自己机子的实际情况进行修改。
此处`MOLCAS_NPROCS=24`表示后续做计算都用24个MPI进程并行，`MOLCAS_MEM=2Gb`亦为每进程内存，
注意这两个环境变量取值与OpenMP版OpenMolcas中不一样)。若/scratch/$USER/molcas目录不存在则
需手动创建。无论计算任务正常/异常结束，该目录都会有临时文件存在，每隔一段时间应进行清理。
若一个任务算完后在相同位置再次提交，OpenMolcas默认去寻找上次的临时文件，好处是可以加速计
算，坏处是万一不想要临时文件里的轨道作为初猜，就可能把自己坑了。若想每次计算完自动清空临
时文件，可以再加上环境变量
```
export MOLCAS_KEEP_WORKDIR=NO
```
变量MOLCAS_PRINT=3可以让输出内容更多一些，偶尔有小伙伴向笔者反映他们的输出内容比我少，往
往就是这个参数造成的。变量MOLCAS_NPROCS用于指定MPI并行核数。这些环境变量仅是笔者的个人推
荐，并非适用于任何机器，详细的环境变量说明请阅读OpenMolcas手册
```
https://molcas.gitlab.io/OpenMolcas/Manual.pdf
```

# 3.测试
执行
```
pymolcas --version
```
屏幕应当显示
```
python driver version = py2.23
(after the original perl EMIL interpreter of Valera Veryazov)
```
接着测试程序自带标准示例。最好切换到其他目录进行测试
```
cd ~/
pymolcas verify standard
```
这个standard其实对应OpenMolcas-v22.10/test/standard目录，内含102个输入文件，可供自学
模仿使用。测试过程中输出内容仅有一行，例如
```
Running test standard: 067... (68%)
```
测试时间可能长达1小时。如果嫌测试时间长，可以自己挑十几个文件测试，例如测试第004，005号文件的命令是
```
pymolcas verify standard:004,005
```

# 4.OpenMP版与MPI版Openmolcas计算速度比较
To be added.

## 相关阅读
1. [离线安装OpenMolcas-v22.06](https://mp.weixin.qq.com/s/R7O2WHSBaxtVi0-aZmutow)  
2. [量子化学程序OpenMolcas的简易安装(低于v22.02)](https://mp.weixin.qq.com/s/wA8YClRxkRTChtQvuum-Uw)  
3. [离线编译OpenMolcas+QCMaquis](https://mp.weixin.qq.com/s/Gb1Lzv1bcQmvuHMZjAQLxQ)  
4. [自动做多参考态计算的程序MOKIT](https://mp.weixin.qq.com/s/bM244EiyhsYKwW5i8wq0TQ)  
5. [利用MOKIT从PySCF向其他量化程序传轨道](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E5%88%A9%E7%94%A8MOKIT%E4%BB%8EPySCF%E5%90%91%E5%85%B6%E4%BB%96%E9%87%8F%E5%8C%96%E7%A8%8B%E5%BA%8F%E4%BC%A0%E8%BD%A8%E9%81%93.md)  
6. [利用MOKIT从Gaussian向其他量化程序传轨道](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E5%88%A9%E7%94%A8MOKIT%E4%BB%8EGaussian%E5%90%91%E5%85%B6%E4%BB%96%E9%87%8F%E5%8C%96%E7%A8%8B%E5%BA%8F%E4%BC%A0%E8%BD%A8%E9%81%93.md)

